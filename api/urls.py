from api.views import DetectPatternView
from django.urls import path

app_name = "api"

urlpatterns = [
    path("detect_pattern/<ticker_symbol>", DetectPatternView.as_view(), name="detect_pattern")
]
