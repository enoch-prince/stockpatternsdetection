import finnhub
from api.serializers import TickerExchangeSerializer
from pattern_recognition.django_files.models import Company
from pattern_recognition.django_files.serializers import CompanySerializer
from pattern_recognition.pr_files.utils.fh_utils import convert_error_to_dict, fh_scan
from pattern_recognition.pr_files.utils.yf_utils import yf_scan
from rest_framework.generics import GenericAPIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status
from django.utils import timezone
from django.db.utils import IntegrityError
import json


class DetectPatternView(GenericAPIView):

    serializer_class = TickerExchangeSerializer

    def get(self, request: Request, ticker_symbol: str) -> Response:
        
        serialized = self.serializer_class(
            data ={"ticker": ticker_symbol}
        )

        if not serialized.is_valid():
            return Response(
                {"error": serialized.errors}, 
                status=status.HTTP_400_BAD_REQUEST
            )

        ticker = serialized.data.get("ticker")
        queryset = Company.objects.filter(ticker=ticker, updated_at__gte=timezone.now().date())
        result = None
        if queryset.exists():
            ser_data = CompanySerializer(queryset[0])
            result = ser_data.data
        else:
            
            try:
                # find the patterns and indicators using a 3rd party api and save it in the db
                patterns = fh_scan(ticker)
            except finnhub.FinnhubAPIException as e:
                # Likely a 429 error is raised so pass on control to the YF SCAN
                print(convert_error_to_dict(str(e)))
                patterns = {}
            
            #breakpoint()
            if not patterns:
                patterns = yf_scan(ticker)
                if not patterns:
                    return Response(
                        {"error": f"{ticker} records couldn't be found"},
                        status=status.HTTP_404_NOT_FOUND
                    )
            try:
                #breakpoint()
                patterns = json.dumps(patterns)
                company, _ = Company.objects.update_or_create(
                    ticker=ticker,
                    defaults= {'details': patterns, "updated_at": timezone.now()}
                )
                company_ser = CompanySerializer(company)
                result = company_ser.data

            except IntegrityError as e:
                print(convert_error_to_dict(str(e)))
                return Response(patterns, status=status.HTTP_200_OK)

        return Response(result, status=status.HTTP_200_OK)
