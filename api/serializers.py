from rest_framework.exceptions import ValidationError
from rest_framework import serializers



class TickerExchangeSerializer(serializers.Serializer):
    ticker = serializers.CharField(max_length=20, min_length=1)

    class Meta:
        fields = ["ticker"]
    
    def validate(self, attrs: dict[str, str]) -> dict[str, str]:
        ticker = attrs.get("ticker")
        if not ticker:
            raise ValidationError("ticker symbol not given. Should be in the form {'ticker':'symbol'}")
        
        if ticker.islower():
            attrs["ticker"] = ticker.upper()
                     
        return attrs
