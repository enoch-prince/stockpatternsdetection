from django.db import models
from django.utils.translation import ugettext_lazy as _
 

class Company(models.Model):
    ticker = models.CharField(_("Ticker Symbol"), max_length=10, unique=True)
    details = models.JSONField(
        _("Details of patterns and indicators detected"), 
        default=dict, blank=True, max_length=1000
    )
    updated_at = models.DateField(_("Last Updated"), auto_now_add=True)

    def __str__(self) -> str:
        return f"{self.ticker} - {self.updated_at}"