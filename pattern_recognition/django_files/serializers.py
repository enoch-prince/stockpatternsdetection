from pattern_recognition.django_files.models import Company
from rest_framework import serializers



class CompanySerializer(serializers.ModelSerializer):
    details = serializers.JSONField()

    class Meta:
        model = Company
        fields = ["ticker", "details", "updated_at"]