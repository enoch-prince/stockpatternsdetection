from __future__ import annotations
import pattern_recognition.pr_files.utils.yf_utils as YF_Utils
from pattern_recognition.pr_files.constants import indicators as INDICATORS
import finnhub
from django.conf import settings
from enum import Enum, auto
from django.utils.timezone import datetime, timedelta

class ResolutionChangeFrequency(Enum):
    """Enum definition for Resolution parameter for finnhub client methods 
    """
    _1_MIN = "1"
    _5_MINS = "5"
    _15_MINS = "15"
    _30_MINS = "30"
    _1_H = "60"
    DAY = "D"
    WEEK = "W"
    MONTH = "M"

class TimeWindow(Enum):
    """Enum for defining the time window for determining Technical Indicators"""
    LAST_N_MINUTES = auto()
    LAST_N_DAYS = auto()
    LAST_N_WEEKS = auto()
    LAST_N_MONTHS = auto()


class TimeWindowToUnixTimestamps:
    """Class for converting TimeWindow into unix timestamps
    """
    WEEKS_IN_A_MONTH = 4.34524

    @staticmethod
    def convert(
        time_window: TimeWindow, 
        N: int
    ):
        """
        Arguments:
            time_window: TimeWindow
            N: int 

        Returns:
            _from: Unix timestamp (int type)
            to: Unix timestamp (int type)
        """

        if time_window is TimeWindow.LAST_N_DAYS:
            t_delta = timedelta(days=N)
        elif time_window is TimeWindow.LAST_N_MONTHS:
            t_delta = timedelta(
                weeks=N*TimeWindowToUnixTimestamps.WEEKS_IN_A_MONTH
            )
        elif time_window is TimeWindow.LAST_N_WEEKS:
            t_delta = timedelta(weeks=N)
        else:
            t_delta = timedelta(minutes=N)
        
        now = datetime.now()
        t_before = now - t_delta

        to = now.timestamp()
        _from = t_before.timestamp()
        return (int(_from), int(to))

class APIKeyType(Enum):
    """Enum definition for the type of API Keys Finnhub offers"""
    SANDBOX = auto()
    MAIN = auto() 

class APIKeyMissingException(Exception):
    """Custom Exception for missing API Key(s) in setting"""
    
    def __init__(self, message: str, *args: object) -> None:
        super().__init__(*args)
        self.message = message
    
    def __str__(self):
        return f"APIKeyMissingException: {self.message}"

class UnknownIndicatorException(Exception):
    """Custom Exception for Unkown Indicator"""
    
    def __init__(self, message: str, *args: object) -> None:
        super().__init__(*args)
        self.message = message
    
    def __str__(self):
        return f"UnknownIndicatorException: {self.message}"

class FHClient(finnhub.Client):
    """Subclassed client for Finnhub api"""

    def __init__(self, which_api: APIKeyType = APIKeyType.SANDBOX, timeout: int = 30) -> None:
        self.DEFAULT_TIMEOUT = timeout
        try:
            if not which_api is APIKeyType.SANDBOX:
                api_key = settings.FINNHUB_API_KEY
            else:
                api_key = settings.FINNHUB_SANDBOX_API_KEY
        except Exception as e:
            raise APIKeyMissingException("API Key is missing in settings")

        super().__init__(api_key=api_key)


fh_sandbox_client = FHClient()
fh_main_client = FHClient(APIKeyType.MAIN)

def convert_from_unixtime(unixtime: int | str):
    if type(unixtime) is str:
        if not unixtime.isdecimal():
            raise ValueError("unixtime must be an integer or decimal in string format")
        unixtime = int(unixtime)
    return datetime.utcfromtimestamp(unixtime).strftime('%Y-%m-%d %H:%M:%S')

def convert_results(patterns: dict[str, list[dict[str,str]]]):
    pattern_list = patterns.get('points')
    if not pattern_list:
        return []

    result =  [
        {
            pattern['patternname']: pattern['patterntype']
            
        } 
        for pattern in pattern_list if pattern['status'] == 'successful'
    ]

    return result

def convert_error_to_dict(err: str):
    return {
        "error": err
    }

def fh_find_pattern(
    client: FHClient,
    ticker: str,
    resolution: ResolutionChangeFrequency
):
    
    try:
        pattern = client.pattern_recognition(ticker, resolution.value)
        return convert_results(pattern)
    except finnhub.FinnhubRequestException:
        return []
    except finnhub.FinnhubAPIException:
        return []


def scan_indicators(
    indicators: dict[str,str], 
    client: FHClient, ticker: str,
    resolution: ResolutionChangeFrequency,
    _from: int, to: int
):  
    """Calculates the values of specific sets of indicators

    Args:
        indicators (dict[str,str]): A dictionary of indicators
        client (FHClient): A Finnhub API Client
        ticker (str): A ticker symbol 
        resolution (ResolutionChangeFrequency): Resolution change frequency of get data
        _from (int): UNIX timestamp. Interval initial value.
        to (int): UNIX timestamp. Interval end value.

    Returns:
        list[dict]:
            [
                {
                    "indicator": calculated-value,
                    "date": date
                }
            ]
    """
    failed_indicators = []
    result = []
    for indicator, indicator_name in indicators.items():
        
        # call finnhub technical indicator endpoint
        try:
            response = client.technical_indicator(
                symbol=ticker, resolution=resolution.value,
                _from=_from, to=to, indicator=indicator,
                indicator_fields={"timeperiod": 3}
            )

        except finnhub.FinnhubAPIException as e:
            error = str(e)
            if "Unknown indicator" in error:
                print(error)
                # mark that indicator and pass it on to another api
                failed_indicators.append((indicator, indicator_name))
            else:
                # recall the API but with increased resolution span
                resolution_span = 30
                _from, to = calculate_unixtimes(resolution, resolution_span)
                response = client.technical_indicator(
                    symbol=ticker, resolution=resolution.value,
                    _from=_from, to=to, indicator=indicator,
                    indicator_fields={"timeperiod": 3}
                )
        else:
            # remove unnecessary keys from the response and get the current
            # figure (last index) of the value (which is a list) whose key is needed
            value = {
                key:val[-1] for key, val in response.items() 
                if key not in ['c', 'h', 'l', 'o', 's', 't', 'v']
            }
            
            # get only the value of key is the same as the indicator (just preference)
            indicator = indicator.lower()
            if indicator in value:
                value = value.get(indicator)


            result.append({
                indicator_name: value
            })

    if failed_indicators:
        # handle failed indicators using Yahoo finance
        for failed_indicator in failed_indicators:
            data = YF_Utils.get_history(failed_indicator[0], YF_Utils.PERIOD)
            YF_Utils.rename_data_columns(data)
            if not data.empty:
                result.append(
                    YF_Utils.process_indicator(
                        data, failed_indicator[0], failed_indicator[1]
                    )
                )
            
    return result


def fh_scan_all_indicators(
    client: FHClient,
    ticker: str,
    resolution: 
        ResolutionChangeFrequency = ResolutionChangeFrequency.DAY,
    resolution_span : int = 15,
):
    """Calculates the values for all inidicators specified

    Args:
        ticker (str): Ticker symbol
        resolution (ResolutionChangeFrequency, optional):
            Resolution change frequency of get data. 
            Defaults to ResolutionChangeFrequency.DAY.
        resolution_span (int, optional): 
            Resolution time span. Defaults to 15 
                i.e The last 15 days,weeks,months etc from the current date.
        use_sandbox_client (bool, optional): 
            The client type to use for the Finnhub API. Defaults to True.

    Returns:
        list: A list of indicators
    """
    
    _from, to = calculate_unixtimes(resolution, resolution_span)

    momentum_indicator = scan_indicators(
        INDICATORS.momentum_indicators,
        client, ticker, resolution, _from, to
    )

    overlap_indicator = scan_indicators(
        INDICATORS.overlap_indicators,
        client, ticker, resolution, _from, to
    )

    volatility_indicator = scan_indicators(
        INDICATORS.volatility_indicators,
        client, ticker, resolution, _from, to
    )

    return [
        *momentum_indicator,
        *volatility_indicator,
        *overlap_indicator,
    ]

def calculate_unixtimes(
    resolution: ResolutionChangeFrequency, 
    resolution_span: int
):
    if resolution is ResolutionChangeFrequency.DAY:
        time_window = TimeWindow.LAST_N_DAYS
    elif resolution is ResolutionChangeFrequency.WEEK:
        time_window = TimeWindow.LAST_N_WEEKS
    elif resolution is ResolutionChangeFrequency.MONTH:
        time_window = TimeWindow.LAST_N_MONTHS
    else:
        time_window = TimeWindow.LAST_N_MINUTES
    
    _from, to = TimeWindowToUnixTimestamps.convert(time_window, resolution_span)
    
    return _from,to

    
def fh_scan(
    ticker: str, 
    resolution: ResolutionChangeFrequency = ResolutionChangeFrequency.DAY, 
    resolution_span: int = 15, use_sandbox_client: bool = True
):
    
    client = fh_main_client if not use_sandbox_client else fh_sandbox_client
    
    patterns = fh_find_pattern(client, ticker, resolution)
    # return an empty dict if no patterns are found
    # it will pass control to the yfinance algorithm
    if not patterns:
        return {}

    indicators = fh_scan_all_indicators(
        client, ticker, resolution, resolution_span
    )

    return {
        "patterns": patterns,
        "indicators": indicators
    }
