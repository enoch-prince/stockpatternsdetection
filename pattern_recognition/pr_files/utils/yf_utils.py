from __future__ import annotations

from pattern_recognition.pr_files.constants import candlesticks, indicators
from typing import Any, Callable
import yfinance as yf
from pandas import DataFrame
import numpy as np
from talib import abstract as talibAbstract
from talib import MAVP as talibMAVP

# valid periods: 1d,5d,1mo,3mo,6mo,1y,2y,5y,10y,ytd,max
PERIOD = "3mo"

def get_history(ticker: str, period: str) -> DataFrame:
    company_ticker = yf.Ticker(ticker)
    return company_ticker.history(period=period)

def convert_history_to_list(history: DataFrame) -> list:
    indeces = list(history)
    new_history = []
    for timestamp in indeces:
        single_history = history[timestamp]
        single_history["Timestamp"] = timestamp
        new_history.append(single_history)
    return new_history

def is_bearish_engulfing(candles: list[dict], index: int) -> bool:
    current_day = candles[index]
    previous_day = candles[index-1]

    if is_bullish_candlestick(previous_day) \
        and float(current_day["Open"]) > float(previous_day["Close"]) \
        and float(current_day["Close"]) < float(previous_day["Open"]):
        return True
    
    return False

def is_bullish_engulfing(candles: list[dict], index: int) -> bool:
    current_day = candles[index]
    previous_day = candles[index-1]

    if is_bearish_candlestick(previous_day) \
        and float(current_day["Close"]) > float(previous_day["Open"]) \
        and float(current_day["Open"]) < float(previous_day["Close"]):
        return True
    
    return False

def is_bullish_candlestick(candle: dict[str, str]) -> bool:
    return float(candle["Close"]) > float(candle["Open"])


def is_bearish_candlestick(candle: dict[str, str]) -> bool:
    return float(candle["Close"]) < float(candle["Open"])

def convert_pattern_results(*result):
    symbol, pattern_type, pattern_name, timestamp = result
    return {
        'symbol': symbol,
        'pattern_type': pattern_type,
        'pattern_name': pattern_name,
        'timestamp': timestamp
    }

def yf_find_pattern(ticker: str, period: str = PERIOD) -> list[dict[str, Any]] | None:
    history = get_history(ticker, period).to_dict('index')
    if not history:
        # the ticker type wasn't found
        return None

    history = convert_history_to_list(history)

    patterns = {
        "bullish": {"detected":False, "detail": []},
        "bearish": {"detected":False, "detail": []}
    }

    for i in range(1, len(history)):
        if is_bullish_engulfing(history, i):
            patterns["bullish"]["detected"] = True
            patterns["bullish"]["detail"].append(f"{history[i]['Timestamp']} - bullish engulfing")
        elif is_bearish_engulfing(history, i):
            patterns["bearish"]["detected"] = True
            patterns["bearish"]["detail"].append(f"{history[i]['Timestamp']} - bearish engulfing")
    
    results = []
    if patterns["bullish"]["detected"]:
        results.append(
            convert_pattern_results(
                ticker, "bullish", "Bullish Engulfing", 
                patterns["bullish"]["detail"]
        ))
    if patterns["bearish"]["detected"]:
        results.append(
            convert_pattern_results(
                ticker, "bearish", "Bearish Engulfing", 
                patterns["bearish"]["detail"]
        ))
    # convert_pattern_results(ticker, "bullish", "Bullish Engulfing", history[i]['Timestamp'])
    return results
    
## ------------------------------------------------------------------------------------------------------------ ##

class PatternDetectionError(Exception):
    """Custom Exception for failed Pattern Detection"""
    
    def __init__(self, message: str, *args: object) -> None:
        super().__init__(*args)
        self.message = message
    
    def __str__(self):
        return f"PatternDetectionError: {self.message}"

class IndicatorScanException(Exception):
    """Custom Exception for failed Indicator Scan"""
    
    def __init__(self, message: str, *args: object) -> None:
        super().__init__(*args)
        self.message = message
    
    def __str__(self):
        return f"IndicatorScanException: {self.message}"

def scan_patterns(data: DataFrame):
    rename_data_columns(data)
    result = []
    for pattern, pattern_name in candlesticks.patterns.items():
        # get the corresponding function of the pattern from talib
        pattern_function = getattr(talibAbstract, pattern)  
        pattern_type = {
            pattern_name: False
        }
        try:
            current_days_data = pattern_function(data)
            
            current_days_value = current_days_data.tail(1).values[0]

            if current_days_value > 0:
                pattern_type[pattern_name] = "bullish"
            elif current_days_value < 0:
                pattern_type[pattern_name] = "bearish"
            else:
                pass

            result.append(pattern_type)

        except Exception as e:
            raise PatternDetectionError(str(e))
    
    return result


def check_convert_int32_type(value):
    if type(value) is np.ndarray:
        # needed to make it JSON serializable
        value = [int(val) if type(val) is np.int32 else val for val in value]
    elif type(value) is np.int32:
        value = int(value)
    return value


def handle_indicator_with_tuple_output(
    indicator_name: str,
    indicator_function: Callable, 
    args: DataFrame
):
    data = indicator_function(args).tail(1)
    values = {}
    for index_name, index_obj in data.items():
        values.update({
            index_name: check_convert_int32_type(index_obj.values[0])
        })

    return {indicator_name: values}


def handle_mavp_indicator(
    data: DataFrame, 
    indicator_name: str, 
):
    data.reset_index(drop=False, inplace=True)
    periods = data.Date
    mavp = talibMAVP(data.close, periods=periods)
    # Get the most recent values (last 5 days) and reverse sort them
    current_mavp = mavp.tail().sort_index(ascending=False).tolist()
    return {indicator_name: current_mavp}


def process_indicator(data: DataFrame, indicator: str, indicator_name: str):
    indicator_function = getattr(talibAbstract, indicator)
    indicator_type = {}
    try:
        if indicator in [
                "AROON", "MACD", "MACDEXT", "MACDFIX",
                "STOCH", "STOCHF", "STOCHRSI"
            ]:
            indicator_type = handle_indicator_with_tuple_output(
                    indicator_name, indicator_function, data
                )

        elif indicator == "MAVP":
            # handle MAVP differently
            indicator_type = handle_mavp_indicator(data, indicator_name)

        else:
            current_days_data = indicator_function(data).tail(1)

            value = current_days_data.values[0]
            value = check_convert_int32_type(value)
            
            current_days_value = value

            indicator_type[indicator_name] = current_days_value
            
        return indicator_type

    except Exception as e:

        raise IndicatorScanException(
            f"{indicator_name} indicator scan failed!\n"  + str(e)
        )


def scan_indicators(data: DataFrame, indicators: dict[str, str]):
    result = []
    for indicator, indicator_name in indicators.items():
        # get the corresponding function of the pattern from talib
        try:
            indicators = process_indicator(data, indicator, indicator_name)
        except IndicatorScanException as e:
            print(str(e))
        else:
            result.append(indicators)
    
    return result



def rename_data_columns(data) -> None:
    """Rename the columns of a dataframe returned by the get_history function. 
       It sets DataFrame's inplace parameter to true.

    Args:
        data (DataFrame): The result of the get_history function
    
    Returns:
        None
    """
    data.rename(
        columns={
            "Open":"open", "High":"high", 
            "Low":"low", "Close": "close",
            "Volume": "volume"
        }, inplace=True
    )

def scan_all_indicators(data: DataFrame):
    
    momentum_indicators = scan_indicators(data, indicators.momentum_indicators)
    overlap_indicators = scan_indicators(data, indicators.overlap_indicators)
    volatility_indicators = scan_indicators(data, indicators.volatility_indicators)
    cycle_indicators = scan_indicators(data, indicators.cycle_indicators)

    # return a combined results using the splat operator
    return [
        *momentum_indicators,
        *volatility_indicators,
        *overlap_indicators,
        *cycle_indicators,
    ]


def yf_scan(
    ticker: str, 
    period: str = PERIOD
) -> dict[str, str | list[dict[str, str | float]]]:
    data = get_history(ticker, period)
    
    if data.empty:
        return {}

    rename_data_columns(data)

    pattern_result = scan_patterns(data)
    indicator_results = scan_all_indicators(data)

    return {
        "patterns": pattern_result,
        "indicators": indicator_results
    }
