#!/usr/bin/env bash
CUSTOM_LIB_PATH=$HOME/custom-libs
tar -xvf extras/ta-lib-0.4.0-src.tar.gz
cd ta-lib/
./configure --prefix=$CUSTOM_LIB_PATH
make
make install
cd ..
export TA_LIBRARY_PATH=$CUSTOM_LIB_PATH/lib
export TA_INCLUDE_PATH=$CUSTOM_LIB_PATH/include