# Stock and Exchange Detection

A simple Django- and DRF-based web service that runs algorithms to detect patterns in historical data for stocks and exchanges.

## Pre-requisite Installation

- Install the latest version of [python](https://www.python.org/downloads/)

- Install _pipenv_ from the command line using the command:

  ```bash
  $ pip install --user pipenv
  ```

- Clone the repository or download the repository and the navigate to the repository on the command line.

- Install _current versions_ of dependencies for the project:

  ```bash
  $ pipenv install
  ```

  or to install the _actual versions_ of dependencies used:

  ```bash
  $ pipenv sync
  ```

## Installing TA-Lib Dependency

- For Windows platforms, run the command:
  ```bash
  $ pipenv install extras/TA_Lib-0.4.21-cp39-cp39-win_amd64.whl
  ```

- For Linux platforms, run the bash script:
  ```bash
  $ chmod +x extras/ta_lib_setup.sh
  ```
  Then:
  ```bash
  $ ./extras/ta_lib_setup.sh
  ```
  Finally, you install the python wrapper for TA-Lib:
  ```bash
  $ pipenv install ta-lib
  ```

- If hosting on pythonanywhere.com, run the bash script in the console:
  ```bash
  $ chmod +x extras/ta_lib_setup_pa.sh
  ```
  Then:
  ```bash
  $ ./extras/ta_lib_setup_pa.sh
  ```

  Finally, you install the python wrapper for TA-Lib:
  ```bash
  $ pipenv install ta-lib
  ```

  **Note**

    You'll have disk space issues when installing TA-Lib on pythonanywhere.com with the free subscription. You'll have to upgrade to get more disk space.


## Environment Configuration

Inside the **core** directory, rename the _**".env.example"**_ to _**".env"**_ and edit the file as appropriate.


## Running the Django App

- Activate the virtual environement created by _pipenv_ by typing in the command line:

  ```bash
   $ pipenv shell
  ```

- Run the server:
  ```bash
  $ cd stockpatternsdetection
  $ python manage.py makemigrations
  $ python manage.py migrate
  $ python manage.py runserver
  ```

## API Endpoints

A list of the implemented API endpoints are listed below:

- **/api/detect_pattern/<ticker_symbol.ticker_exchange>** - _GET request to get the list of patterns detected for the specified ticker and exchange._

- **/api/detect_pattern/<ticker_symbol>** - _GET request that lists the patterns detected for the specified ticker symbol._

## Contributing

Please make a pull request if you want to contribute to this project

## License

MIT License
